
import time
timeStart = time.perf_counter()
def saxpy(a, n):
    result = []
    x = [i for i in range(n)]
    y = [i for i in range(n)]
    for i in range(0, n):
        result.append(a * x[i] + y[i])
    return result
res = saxpy(1, 50)
print(res)
timeEnd = time.perf_counter() - timeStart
print(f"Время работы: {timeEnd}")
